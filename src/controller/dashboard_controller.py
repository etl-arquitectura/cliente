##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: dashboard_controller.py
# Capitulo: Flujo de Datos
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Version: 1.0.0 Noviembre 2022
# Descripción:
#
#   Este archivo define la funcionalidad del componente
#
#-------------------------------------------------------------------------
# Version: 2.0.0 Marzo 2023
# Autor(es): Ulises Huerta, Juan Navarro, Alexis Ultreras y Reyna Sanchez
#-------------------------------------------------------------------------


from src.data.repository import Repository
import json
from datetime import datetime

class DashboardController:

    @staticmethod
    def load_products():
        response = Repository.get_products()
        if response.status_code != 200:
            return {"products": 0} 
        
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        return {
            "products": json_response["data"]["response"][0]["count"]
        }

    @staticmethod
    def load_providers():
        response = Repository.get_providers()
        if response.status_code != 200:
            return {"providers": 0}
        
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        return {
            "providers": json_response["data"]["response"][0]["count"]
        }

    @staticmethod
    def load_locations():
        response = Repository.get_locations()
        if response.status_code != 200:
            return {"locations": 0}
        
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        return {
            "locations": json_response["data"]["response"][0]["count"]
        }

    @staticmethod
    def load_orders():
        response = Repository.get_orders()
        if response.status_code != 200:
            return {"orders": 0}
        
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        return {
            "orders": json_response["data"]["response"][0]["count"]
        }

    @staticmethod
    def load_sales():
        response = Repository.get_sales()
        if response.status_code != 200:
            return {"sales": 0}
        
        json_response = json.loads(response.text)
        
        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        return {
            "sales": json_response["data"]["response"][0]["total"]
        }

    @staticmethod
    def load_providers_per_location():
        response = Repository.get_providers_by_location()
        if response.status_code != 200:
            return {
                "providers": [],
                "location": []
            }
        result = {
            "providers": [],
            "location": []
        }

        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        for entry in json_response["data"]["response"]:
            result["providers"].append(entry["providers"][0]["count"])
            result["location"].append(entry["name"])
        return result

    @staticmethod
    def load_sales_per_location():
        response = Repository.get_sales_by_location()
        if response.status_code != 200:
            return {
                "sales": [],
                "location": []
            }
        result = {
            "sales": [],
            "location": []
        }
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        for entry in json_response["data"]["response"]:
            result["location"].append(entry["name"])
            total = 0
            for sold in entry["providers"]:
                for order in sold["sold"]:
                    total += (int(order["quantity"]) * float(order["quantity"]))
            result["sales"].append(total)
            
        return result

    @staticmethod
    def load_orders_per_location():
        response = Repository.get_orders_by_location()
        if response.status_code != 200:
            return {
                "orders": [],
                "location": []
            }
        result = {
            "orders": [],
            "location": []
        }
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        for entry in json_response["data"]["response"]:
            result["location"].append(entry["name"])
            total = 0
            for sold in entry["providers"]:
                total += int(sold["sold"])
            result["orders"].append(total)
        return result

    @staticmethod
    def load_best_sellers():
        response = Repository.get_best_sellers()
        if response.status_code != 200:
            return []
        result = []
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        for product in json_response["data"]["response"][0:5]:
            result.append({
                "invoice": product["times"],
                "total": int(product["times"]) * float(product["price"])
            })
        return result

    @staticmethod
    def load_worst_sales():
        response = Repository.get_worst_sales()
        if response.status_code != 200:
            return []
        result = []
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        for product in json_response["data"]["response"][0:5]:
            result.append({
                "invoice": product["times"],
                "total": int(product["times"]) * float(product["price"])
            })
        return result

    @staticmethod
    def load_most_selled_products():
        response = Repository.get_most_selled_products()
        if response.status_code != 200:
            return []
        result = []
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        for product in json_response["data"]["response"][0:5]:
            result.append({
                "product": product["description"],
                "times": product["times"]
            })
        return result
    
    @staticmethod
    def load_sales_by_year():
        response = Repository.get_sales_by_year()
        if response.status_code != 200:
            return []
        
        result = []
        years = [] 
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        # Hacemos el filtrado por año
        for order in json_response["data"]["response"]:
            dt = order["date"][0:4]
            if dt not in years:
                dicc = {
                    "anio": dt,
		            "total_de_ventas" : order["total"]
                }
                years.append(dt)
                result.append(dicc)
            else:
                pos = years.index(dt)
                result[pos]["total_de_ventas"] += order["total"]
                            
        return result
    
    @staticmethod
    def load_number_of_sold_products_by_years():
        response = Repository.get_number_of_sold_products_by_years()
        if response.status_code != 200:
            return []
        
        result = []
        years = [] 
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        # Hacemos el filtrado por año
        for order in json_response["data"]["response"]:
            dt = order["date"][0:4]
            if dt not in years:
                dicc = {
                    "year": dt,
		            "total_amount_of_sold_products" : order["quantity"],
                    "amount_of_orders": 1
                }
                years.append(dt)
                result.append(dicc)
            else:
                pos = years.index(dt)
                result[pos]["total_amount_of_sold_products"] += order["quantity"]
                result[pos]["amount_of_orders"] += 1
                            
        return result
    
    @staticmethod
    def load_sales_of_providers_by_years():
        response = Repository.get_sales_of_providers_by_years()
        if response.status_code != 200:
            return []
        
        result = []
        info_by_years = [] 
        countries = []
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        
        countries = {}
        for country in json_response["data"]["response"]:
            dicc = {}
            for c_prov in country["providers"]:
                for c_sold in c_prov["sold"]:
                    for c_bougth in c_sold["bought"]:
                        if c_bougth["date"][0:4] not in info_by_years:
                            dicc[c_bougth["date"][0:4]]=1
                            info_by_years.append(c_bougth["date"][0:4])
                        else:
                            dicc[c_bougth["date"][0:4]] += 1
            countries[country['name']]=dicc
            info_by_years.clear()
        return countries

    @staticmethod
    def load_most_sold_products_by_years():
        response = Repository.get_most_sold_products_by_years()
        if response.status_code != 200:
            return []
        
        json_response = json.loads(response.text)

        assert('data' in json_response.keys())
        assert('response' in json_response['data'].keys())

        years = []
        data = []
        for product in json_response["data"]["response"]:
            for current_product_boughts in product["bought"]:
                if current_product_boughts["date"][0:4] not in years:
                    years.append(current_product_boughts["date"][0:4])
                    tmp_arr = []
                    tmp_pair = []

                    tmp_pair.append(product["description"])
                    tmp_pair.append(1)

                    tmp_arr.append(tmp_pair)
                    data.append(tmp_arr)
                else:
                    pos_year = years.index(current_product_boughts["date"][0:4])
                    pos_last_element = len(data[pos_year])-1
                    if data[pos_year][pos_last_element][0] == product["description"]:
                        data[pos_year][pos_last_element][1] += 1
                    else:
                        tmp_pair = []
                        tmp_pair.append(product["description"])
                        tmp_pair.append(1)

                        data[pos_year].append(tmp_pair)
        # Sorting
        for i in range(0,len(years)):
            datat = sorted(data[i], key=lambda x: x[1], reverse=True)
            data[i] = datat

        for i in range(0,len(data)):
            top_5_sold_products = []
            for j in range(0,5):
                top_5_sold_products.append(data[i][j])
            data[i] = top_5_sold_products

        return {"years": years, "data":data}
